package com.catho.jee;

import com.catho.jee.Dao.AdherentCrud;
import com.catho.jee.Entity.AdherentEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();
        try {
            AdherentCrud adherentManager = new AdherentCrud();
            adherentManager.setSessionFactory(session);

            // Test 1 : Ajout d'un adhérent
            Transaction tx = session.beginTransaction();
            AdherentEntity adherent1 = new AdherentEntity(1000, "Marcel",
                    "Patulacci", "marcel.patulaci@laposte.net",
                    "20/02/1995", "16/03/2019", null);

            adherentManager.persist(adherent1);
            tx.commit();

            System.out.println(adherentManager.findAll());

            // Test 2 : Update d'un adhérent
            tx = session.beginTransaction();
            adherent1 = session.load(AdherentEntity.class, 1000);
            adherent1.setFirstName("Derrick");
            adherentManager.update(adherent1);
            tx.commit();

            System.out.println(adherentManager.findAll());

            // Test 4 : Find all ordered by date
            AdherentEntity adherent2 = new AdherentEntity(1001, "Lionel",
                    "Lechevalier", "marcel.patulaci@laposte.net",
                    "20/02/1995", "16/04/2019", null);

            AdherentEntity adherent3 = new AdherentEntity(1002, "Jean",
                    "Larmarc", "marcel.patulaci@laposte.net",
                    "20/02/1995", "13/03/2019", null);
            tx = session.beginTransaction();
            adherentManager.persist(adherent2);
            adherentManager.persist(adherent3);
            tx.commit();
            System.out.println(adherentManager.findAll());
            System.out.println(adherentManager.findAllOrderBy("dateSubscription", "DESC"));


            // Test 4 : Delete d'un adhérent
            tx = session.beginTransaction();
            adherent1 = session.load(AdherentEntity.class, 1000);
            adherent2 = session.load(AdherentEntity.class, 1001);
            adherent3 = session.load(AdherentEntity.class, 1002);

            adherentManager.delete(adherent1);
            adherentManager.delete(adherent2);
            adherentManager.delete(adherent3);
            tx.commit();
            System.out.println(adherentManager.findAll());
        } finally {
            session.close();
        }
    }
}
