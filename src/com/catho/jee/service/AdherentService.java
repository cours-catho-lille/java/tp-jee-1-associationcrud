package com.catho.jee.service;

import com.catho.jee.Dao.AdherentCrud;
import com.catho.jee.Entity.AdherentEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class AdherentService {
    // Je ne comprend pas vraiment l'utilité d'un service alors qu'il reprend les fonctions du DAO.

    private static AdherentCrud adherentCrud = new AdherentCrud();
    private static SessionFactory ourSessionFactory = null;
    private static Session session;

    private AdherentService() {
        Configuration configuration = new Configuration();
        configuration.configure();

        ourSessionFactory = configuration.buildSessionFactory();

        session = ourSessionFactory.openSession();
        adherentCrud.setSessionFactory(session);
    }

    public void persist(AdherentEntity entity) {
        Transaction tx = session.beginTransaction();
        adherentCrud.persist(entity);
        tx.commit();
    }

    public void delete(AdherentEntity entity) {
        Transaction tx = session.beginTransaction();
        adherentCrud.delete(entity);
        tx.commit();
    }

    public void update(AdherentEntity entity) {
        Transaction tx = session.beginTransaction();
        adherentCrud.update(entity);
        tx.commit();
    }

    public List<AdherentEntity> listAll() {
        return adherentCrud.findAll();
    }

}
