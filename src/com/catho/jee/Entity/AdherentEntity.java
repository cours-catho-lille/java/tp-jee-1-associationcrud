package com.catho.jee.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "adherent", schema = "Association")
public class AdherentEntity {
    private int id;
    private String firstName;
    private String lastName;
    private String dateSubscription;
    private String dateBirth;
    private String email;
    private Integer hasPaid;
    private Integer cooptation;

    public AdherentEntity(int id, String firstName, String lastName, String email, String dateBirth, String dateSubscription, Integer cooptation) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateSubscription = dateSubscription;
        this.dateBirth = dateBirth;
        this.email = email;
        this.cooptation = cooptation;
    }

    public AdherentEntity(String firstName, String lastName, String email, String dateBirth, String dateSubscription, Integer cooptation) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.cooptation = cooptation;
        this.dateBirth = dateBirth;
        this.dateSubscription = dateSubscription;
    }

    public AdherentEntity() {
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "dateSubscription")
    public String getDateSubscription() {
        return dateSubscription;
    }

    public void setDateSubscription(String dateSubscription) {
        this.dateSubscription = dateSubscription;
    }

    @Basic
    @Column(name = "dateBirth")
    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "hasPaid")
    public Integer getHasPaid() {
        return hasPaid;
    }

    public void setHasPaid(Integer hasPaid) {
        this.hasPaid = hasPaid;
    }

    @Basic
    @Column(name = "cooptation")
    public Integer getCooptation() {
        return cooptation;
    }

    public void setCooptation(Integer cooptation) {
        this.cooptation = cooptation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdherentEntity that = (AdherentEntity) o;
        return id == that.id &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(dateSubscription, that.dateSubscription) &&
                Objects.equals(dateBirth, that.dateBirth) &&
                Objects.equals(email, that.email) &&
                Objects.equals(hasPaid, that.hasPaid) &&
                Objects.equals(cooptation, that.cooptation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, dateSubscription, dateBirth, email, hasPaid, cooptation);
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
