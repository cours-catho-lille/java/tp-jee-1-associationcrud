package com.catho.jee.Dao;

import java.util.List;

public interface Crud<T> {
    void persist(T entity);

    void update(T entity);

    void delete(T entity);

    T findById(int id);

    List<T> findAll();

    List<T> findAllOrderBy(String field, String order);
}
