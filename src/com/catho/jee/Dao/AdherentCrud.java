package com.catho.jee.Dao;

import com.catho.jee.Entity.AdherentEntity;
import org.hibernate.Session;

import java.util.List;

public class AdherentCrud implements Crud {

    private Session session;

    public void setSessionFactory(Session sessionFactory) {
        this.session = sessionFactory;
    }

    @Override
    public void persist(Object entity) {
        session.persist(entity);
    }

    @Override
    public void update(Object entity) {
        session.update(entity);
    }

    @Override
    public void delete(Object entity) {
        session.delete(entity);
    }

    @Override
    public Object findById(int id) {
        AdherentEntity adherent = (AdherentEntity) session.byId(Integer.toString(id));
        return adherent;
    }

    @Override
    public List findAll() {
        return session.createQuery("FROM AdherentEntity").list();
    }

    @Override
    public List findAllOrderBy(String field, String order) {
        return session.createQuery("FROM AdherentEntity ORDER BY " + field + " " + order).list();
    }
}
